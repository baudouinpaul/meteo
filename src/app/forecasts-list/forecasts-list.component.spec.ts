/* tslint:disable:no-unused-variable */
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { WeatherObsService } from 'app/shared/services/weather.service';
import { of } from 'rxjs';
import { ForecastsListComponent } from './forecasts-list.component';


describe('ForecastsListComponent', () => {
    let component: ForecastsListComponent;
    let fixture: ComponentFixture<ForecastsListComponent>;
    let mockWeatherService: any;
    const mockResponse = {
        "city": {
            "id": 0,
            "name": "Blain",
            "coord": {
                "lon": -1.7628,
                "lat": 47.4766
            },
            "country": "FR",
            "population": 0,
            "timezone": 7200
        },
        "cod": "200",
        "message": 22.3904893,
        "cnt": 5,
        "list": [
            {
                "dt": 1666609200,
                "sunrise": 1666593500,
                "sunset": 1666631034,
                "temp": {
                    "day": 63.95,
                    "min": 56.12,
                    "max": 64.29,
                    "night": 56.23,
                    "eve": 59.86,
                    "morn": 58.57
                },
                "feels_like": {
                    "day": 63.72,
                    "night": 56.07,
                    "eve": 59.68,
                    "morn": 58.32
                },
                "pressure": 1011,
                "humidity": 78,
                "weather": [
                    {
                        "id": 500,
                        "main": "Rain",
                        "description": "light rain",
                        "icon": "10d"
                    }
                ],
                "speed": 25.88,
                "deg": 220,
                "gust": 41.5,
                "clouds": 30,
                "pop": 0.89,
                "rain": 6.17
            }
        ]
    };

    beforeEach(async(() => {
        // Mock service
        mockWeatherService = jasmine.createSpyObj(['getForecast', 'getWeatherIcon']);
        mockWeatherService.getForecast.and.returnValue(of(mockResponse));
        mockWeatherService.getWeatherIcon.and.returnValue(of('test/toto.png'));
        // Configuration
        TestBed.configureTestingModule({
            declarations: [ForecastsListComponent],
            imports: [HttpClientModule],
            providers: [
                {
                    provide: WeatherObsService,
                    useValue: mockWeatherService
                },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({
                            zipcode: 44000,
                            countryCode: 'FR'
                        }),
                    }
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ForecastsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should get forecast data when zipCode and countryCode provided', () => {
        expect(component).toBeTruthy();
        expect(mockWeatherService.getForecast).toHaveBeenCalledWith({
            zipCode: 44000,
            countryCode: 'FR'
        });
        expect(component.forecast).toEqual(mockResponse);
    });

    it('should get weather icon path with weather ID', () => {
        expect(component).toBeTruthy();
        expect(mockWeatherService.getWeatherIcon).toHaveBeenCalledWith(500);
    });
});
