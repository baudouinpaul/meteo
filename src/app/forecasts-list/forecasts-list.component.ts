import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryZipModel } from 'app/shared/models/country-zip.model';
import { WeatherObsService } from 'app/shared/services/weather.service';

@Component({
  selector: 'app-forecasts-list',
  templateUrl: './forecasts-list.component.html',
  styleUrls: ['./forecasts-list.component.scss']
})
export class ForecastsListComponent {

  forecast: any;

  constructor(public weatherService: WeatherObsService, route: ActivatedRoute) {
    route.params.subscribe(params => {
      const countryZip: CountryZipModel = {
        zipCode: params['zipcode'],
        countryCode: params['countryCode']
      };
      this.weatherService.getForecast(countryZip)
        .subscribe(data => this.forecast = data);
    });
  }
}
