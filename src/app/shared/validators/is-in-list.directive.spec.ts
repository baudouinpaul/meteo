/* tslint:disable:no-unused-variable */
import { FormControl } from '@angular/forms';
import { isInListValidator } from './is-in-list.directive';


describe('IsInListValidatorTest', () => {
    const validator = isInListValidator(['toto', 'titi', 'tutu']);
    const control = new FormControl('input');

    it('should return null if input string length is less than max', () => {
        control.setValue('toto');
        expect(validator(control)).toBeNull();
    });

    it('should return correct object if input string length is more than max', () => {
        control.setValue('test');
        expect(validator(control)).toEqual({ isNotInList: true });
    });
});