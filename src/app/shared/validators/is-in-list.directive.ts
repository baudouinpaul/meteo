import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

/**
 * Check if control value is included in provided list
 * @param list list of authorized strings
 * @returns null if string is included, isNotInList error is other case
 */
export function isInListValidator(list: any[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const isNotInList = !list.includes(control.value);
        return isNotInList ? { isNotInList: true } : null;
    };
}