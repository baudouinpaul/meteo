export interface AsyncResponseHandlerModel {
    data: any,
    status: ActionStatusEnum,
    message?: any
}

export enum ActionStatusEnum {
    ADDED = 'ADDED', // Add to list and localstorage, with success toaster
    DELETED = 'DELETED', // Remove from list and localstorage, with success toaster
    UPDATED = 'UPDATED', // Update from list, no toaster
    ERROR = 'ERROR' // No action but error toaster
}