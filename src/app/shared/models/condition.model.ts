import { CountryZipModel } from "./country-zip.model";

export interface ConditionModel {
    countryZip: CountryZipModel,
    data: ConditionDataModel,
    icon: string
}

export interface ConditionDataModel {
    coord: CoordModel,
    weather: WeatherModel[],
    base: string,
    main: MainModel,
    visibility: number,
    wind: WindModel,
    clouds: CloudsModel,
    rain: RainModel,
    snow: SnowModel,
    dt: number,
    sys: SysModel,
    timezone: number,
    id: number,
    name: string,
    cod: number
}

export interface CoordModel {
    lon?: number, // City geo location, longitude
    lat?: number // City geo location, latitude
}

export interface WeatherModel {
    id?: number, // Weather condition id
    main?: string, // Group of weather parameters (Rain, Snow, Extreme etc.)
    description?: string, // Weather condition within the group.
    icon?: string // Weather icon id
}

export interface MainModel {
    temp?: number, // Temperature. 
    feels_like?: number, // Temperature. This temperature parameter accounts for the human perception of weather.
    pressure?: number, // Atmospheric pressure(on the sea level, if there is no sea_level or grnd_level data), hPa
    humidity?: number, // Humidity, %
    temp_min?: number, // Minimum temperature at the moment. This is minimal currently observed temperature (within large megalopolises and urban areas).
    temp_max?: number, // Maximum temperature at the moment. This is maximal currently observed temperature (within large megalopolises and urban areas).
    sea_level?: number, // Atmospheric pressure on the sea level, hPa
    grnd_level?: number, // Atmospheric pressure on the ground level, hPa
}

export interface WindModel {
    speed?: number, // Wind speed
    deg?: number, // Wind direction, degrees (meteorological)
    gust?: number, // Wind gust.
}

export interface CloudsModel {
    all?: number // Cloudiness, %
}

export interface RainModel {
    '1h'?: number, // Rain volume for the last 1 hour, mm
    '3h'?: number // Rain volume for the last 3 hour, mm
}

export interface SnowModel {
    '1h'?: number, // Snow volume for the last 1 hour, mm
    '3h'?: number // Snow volume for the last 3 hour, mm
}

export interface SysModel {
    type?: number, // Internal parameter
    id?: number, // Internal parameter
    message?: string, // Internal parameter
    country?: string, // Country code (GB, JP etc.)
    sunrise?: number, // Sunrise time, unix, UTC
    sunset?: number, //  Sunset time, unix, UTC
}