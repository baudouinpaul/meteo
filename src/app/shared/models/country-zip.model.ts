export interface CountryZipModel {
    countryCode: string,
    zipCode: string
}