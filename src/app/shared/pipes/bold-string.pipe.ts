import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'markBold'
})
/**
 * Mark as bold a part of a string, provided as pipe argument.
 * Not case sensitive.
 */
export class MarkBoldPipe implements PipeTransform {

    transform(textVal: string, subTextValue: string): string {
        const start = textVal.toLocaleLowerCase().indexOf(subTextValue.toLocaleLowerCase());
        if (start !== -1) {
            const valueToReplace = textVal.substring(start, start + subTextValue.length);
            return textVal.replace(valueToReplace, '<b>' + valueToReplace + '</b>');
        }
        return textVal;
    }
}