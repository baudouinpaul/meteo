/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StateButtonComponent } from './state-button.component';
import { StateButtonStatusEnum } from './state-button.model';


describe('StateButtonComponent', () => {
  let component: StateButtonComponent;
  let fixture: ComponentFixture<StateButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StateButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test onClick when status is WAITING', () => {
    // With
    component.status = StateButtonStatusEnum.WAITING;
    const spy = spyOn(component.buttonClick, 'emit');
    // When
    component.onClick();
    // Then
    expect(component.status).toEqual(StateButtonStatusEnum.LOADING);
    expect(spy).toHaveBeenCalled();
  });

  it('should test onClick when status is already LOADING', () => {
    // With
    component.status = StateButtonStatusEnum.LOADING;
    const spy = spyOn(component.buttonClick, 'emit');
    // When
    component.onClick();
    // Then
    expect(component.status).toEqual(StateButtonStatusEnum.LOADING);
    expect(spy).not.toHaveBeenCalled();
  });
});
