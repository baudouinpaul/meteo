import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { StateButtonStatusEnum } from './state-button.model';

@Component({
  selector: 'app-state-button',
  templateUrl: './state-button.component.html',
  styleUrls: ['./state-button.component.scss']
})
export class StateButtonComponent {

  @Input() initialTemplate: TemplateRef<any>;
  @Input() workingTemplate: TemplateRef<any>;
  @Input() doneTemplateSuccess: TemplateRef<any>;
  @Input() doneTemplateError: TemplateRef<any>;
  @Input() disabled: boolean;
  @Input() status: StateButtonStatusEnum;
  @Output() buttonClick: EventEmitter<void> = new EventEmitter();

  StateButtonStatusEnum = StateButtonStatusEnum;

  onClick(): void {
    // Only allow user to click when button is at WAITING state
    this.status === StateButtonStatusEnum.WAITING ? this.buttonClick.emit() : null;
    // Switch to LOADING state
    this.status = StateButtonStatusEnum.LOADING;
  }

  getTemplateToShow(): TemplateRef<any> {
    switch (this.status) {
      case StateButtonStatusEnum.WAITING:
        return this.initialTemplate;
      case StateButtonStatusEnum.LOADING:
        return this.workingTemplate;
      case StateButtonStatusEnum.SUCCESS:
        return this.doneTemplateSuccess;
      case StateButtonStatusEnum.ERROR:
        return this.doneTemplateError;
      default:
        return this.initialTemplate;
    }
  }

}
