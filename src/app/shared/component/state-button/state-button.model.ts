export enum StateButtonStatusEnum {
    WAITING = "WAITING",
    LOADING = "LOADING",
    SUCCESS = "SUCCESS",
    ERROR = "ERROR"
}