import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StateButtonComponent } from './component/state-button/state-button.component';
import { MarkBoldPipe } from './pipes/bold-string.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [StateButtonComponent, MarkBoldPipe],
  exports: [StateButtonComponent, MarkBoldPipe]
})
export class SharedModule { }
