import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActionStatusEnum, AsyncResponseHandlerModel } from '../models/async-response-handler.model';
import { ConditionDataModel, ConditionModel } from '../models/condition.model';
import { CountryZipModel } from '../models/country-zip.model';

@Injectable({
    providedIn: 'root'
})
export class WeatherObsService {

    private conditionListSubject = new BehaviorSubject<AsyncResponseHandlerModel>({
        data: [],
        status: null
    });
    private conditions: ConditionModel[] = [];
    static URL = 'https://api.openweathermap.org/data/2.5';
    static APPID = '5a4b2d457ecbef9eb2a71e480b947604';
    static ICON_URL = 'https://raw.githubusercontent.com/udacity/Sunshine-Version-2/sunshine_master/app/src/main/res/drawable-hdpi/';

    constructor(private http: HttpClient) { }

    /**
     * Allows to subscribe to the conditionListSubject subject.
     * @returns Observable of ConditionModel[]
     */
    getConditionsList(): Observable<AsyncResponseHandlerModel> {
        return this.conditionListSubject.asObservable();
    }

    /**
     * Add a ConditionModel to the list, and emit a new conditionListSubject observable.
     * @param countryZip CountryZipModel to add
     */
    addToConditionsList(countryZip: CountryZipModel, calledFromForm: boolean, isUpdating: boolean): void {
        // First, check if do not already exists
        const existingOne = this.conditions.find(condition => condition.countryZip.countryCode === countryZip.countryCode && condition.countryZip.zipCode === countryZip.zipCode);
        const shouldAdd = !existingOne && !isUpdating;
        const shouldUpdate = existingOne && isUpdating;
        if (shouldAdd || shouldUpdate) {
            // Here we make a request to get the curretn conditions data from the API. Note the use of backticks and an expression to insert the zipcode
            this.http.get(`${WeatherObsService.URL}/weather?zip=${countryZip.zipCode},${countryZip.countryCode.toLowerCase()}&units=imperial&APPID=${WeatherObsService.APPID}`)
                .subscribe(
                    (data: ConditionDataModel) => {
                        if (shouldAdd) {
                            this.conditions.push({
                                countryZip: countryZip,
                                data: data,
                                icon: this.getWeatherIcon(data.weather[0].id)
                            });
                        } else {
                            existingOne.data = data;
                            existingOne.icon = this.getWeatherIcon(data.weather[0].id)
                        }
                        // Change values in observable
                        this.conditionListSubject.next({
                            data: this.conditions,
                            status: calledFromForm ? ActionStatusEnum.ADDED : ActionStatusEnum.UPDATED,
                            message: countryZip
                        });
                    },
                    (error) => {
                        this.errorPropagation(`No data found for couple ${countryZip.countryCode} / ${countryZip.zipCode}. Please try somethong else.`);
                    }
                );
        } else {
            this.errorPropagation(`Couple ${countryZip.countryCode} / ${countryZip.zipCode} is already in list.`);
        }

    }

    removeFromConditionsList(conditionToRemove: ConditionModel): void {
        const index = this.conditions.indexOf(conditionToRemove);
        if (index > -1) {
            this.conditions.splice(index, 1);
            this.conditionListSubject.next({
                data: this.conditions,
                status: ActionStatusEnum.DELETED,
                message: conditionToRemove.countryZip
            });
        }
    }

    getForecast(countryZip: CountryZipModel): Observable<any> {
        // Here we make a request to get the forecast data from the API. Note the use of backticks and an expression to insert the zipcode
        return this.http.get(`${WeatherObsService.URL}/forecast/daily?zip=${countryZip.zipCode},${countryZip.countryCode}&units=imperial&cnt=5&APPID=${WeatherObsService.APPID}`);

    }

    errorPropagation(errorMessage?: string): void {
        this.conditionListSubject.next({
            data: this.conditions,
            status: ActionStatusEnum.ERROR,
            message: errorMessage
        });
    }

    getWeatherIcon(id): string {
        if (id >= 200 && id <= 232)
            return WeatherObsService.ICON_URL + "art_storm.png";
        else if (id >= 501 && id <= 511)
            return WeatherObsService.ICON_URL + "art_rain.png";
        else if (id === 500 || (id >= 520 && id <= 531))
            return WeatherObsService.ICON_URL + "art_light_rain.png";
        else if (id >= 600 && id <= 622)
            return WeatherObsService.ICON_URL + "art_snow.png";
        else if (id >= 801 && id <= 804)
            return WeatherObsService.ICON_URL + "art_clouds.png";
        else if (id === 741 || id === 761)
            return WeatherObsService.ICON_URL + "art_fog.png";
        else
            return WeatherObsService.ICON_URL + "art_clear.png";
    }
}
