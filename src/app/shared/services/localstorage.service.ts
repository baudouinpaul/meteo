import { Injectable, OnDestroy } from '@angular/core';
import { CountryZipModel } from '../models/country-zip.model';
import { WeatherObsService } from './weather.service';

export const LOCATIONS: string = "locations";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService implements OnDestroy {

  localStorageCountryZipList: CountryZipModel[] = [];
  polling: any;

  constructor(private weatherService: WeatherObsService) {
    this.updateAllFromLocalStorage(true);
    // Auto refresh ever 30 seconds
    this.polling = setInterval(this.updateAllFromLocalStorage.bind(this), 30000);
  }

  /**
   * Add a couple of country code / zipcode to localStorage.
   * Be sure this couple is valid before to add them.
   * @param countryZip couple of country code and zipcode
   */
  addToLocalStorage(countryZip: CountryZipModel): void {
    // Check if not already present
    const existing = this.localStorageCountryZipList.find(couple => couple.countryCode === countryZip.countryCode && couple.zipCode === countryZip.zipCode);
    if (!existing) {
      this.localStorageCountryZipList.push(countryZip);
      localStorage.setItem(LOCATIONS, JSON.stringify(this.localStorageCountryZipList));
    }
  }

  /**
   * Removes a couple of country code / zipcode of localstorage
   * @param countryZip couple of country code and zipcode
   */
  removeFromLocalStorage(countryZip: CountryZipModel) {
    let index = this.localStorageCountryZipList.indexOf(countryZip);
    if (index !== -1) {
      this.localStorageCountryZipList.splice(index, 1);
      localStorage.setItem(LOCATIONS, JSON.stringify(this.localStorageCountryZipList));
    }
  }

  /**
   * Update all weather informations for couples saved in localStorage
   */
  updateAllFromLocalStorage(isFirstLoad?: boolean): void {
    // Get data in localStorage
    const locString: string = localStorage.getItem(LOCATIONS);
    if (locString)
      // Extract couples of country code and zipcode from localstorage
      this.localStorageCountryZipList = JSON.parse(locString);
    for (let countryZip of this.localStorageCountryZipList)
      // For each couple, get async weather data
      this.weatherService.addToConditionsList(countryZip, false, !isFirstLoad);
  }

  ngOnDestroy(): void {
    if (this.polling) {
      clearInterval(this.polling);
    }
  }
}
