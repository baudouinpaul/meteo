/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConditionModel } from 'app/shared/models/condition.model';
import { MeteoCardsComponent } from './meteo-card.component';


describe('MeteoCardsComponent', () => {
  let component: MeteoCardsComponent;
  let fixture: ComponentFixture<MeteoCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MeteoCardsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteoCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should not crash when no condition', () => {
    expect(component).toBeTruthy();
  });

  it('should emit removeCard event onRemoveCard', () => {
    // With
    const mockedCondition: ConditionModel = {
      countryZip: {
        countryCode: 'FR',
        zipCode: '44000'
      },
      data: null,
      icon: 'test/toto.png'
    };
    component.condition = mockedCondition;
    const spy = spyOn(component.removeCard, 'emit');
    // When
    component.onRemoveCard();
    // Then
    expect(spy).toHaveBeenCalledOnceWith(mockedCondition);
  });

});
