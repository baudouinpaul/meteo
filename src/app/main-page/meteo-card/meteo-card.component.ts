import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ConditionModel } from 'app/shared/models/condition.model';

@Component({
  selector: 'app-meteo-card',
  templateUrl: './meteo-card.component.html',
  styleUrls: ['./meteo-card.component.scss']
})
export class MeteoCardsComponent {

  @Input() condition: ConditionModel;
  @Output() removeCard: EventEmitter<ConditionModel> = new EventEmitter();

  constructor(private router: Router) { }

  onRemoveCard(): void {
    this.removeCard.emit(this.condition);
  }
}
