/* tslint:disable:no-unused-variable */
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StateButtonStatusEnum } from 'app/shared/component/state-button/state-button.model';
import { ActionStatusEnum, AsyncResponseHandlerModel } from 'app/shared/models/async-response-handler.model';
import { ConditionModel } from 'app/shared/models/condition.model';
import { LocalStorageService } from 'app/shared/services/localstorage.service';
import { WeatherObsService } from 'app/shared/services/weather.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { MainPageComponent } from './main-page.component';

describe('MainPageComponent', () => {
    let component: MainPageComponent;
    let fixture: ComponentFixture<MainPageComponent>;
    let mockToastrService = jasmine.createSpyObj(['error', 'info', 'success']);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MainPageComponent],
            imports: [HttpClientModule, ToastrModule],
            providers: [
                { provide: ToastrService, useValue: mockToastrService },
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should test removeCard', () => {
        // With
        const spy = spyOn(WeatherObsService.prototype, 'removeFromConditionsList');
        const condition: ConditionModel = {
            countryZip: {
                countryCode: 'FR',
                zipCode: '33333'
            },
            data: null,
            icon: 'test.jpg'
        };
        // When
        component.removeCard(condition);
        // Then
        expect(spy).toHaveBeenCalledWith(condition);
    });

    describe('should test manageNewConditions', () => {

        it('should test ERROR case', () => {
            // With
            const conditions: AsyncResponseHandlerModel = {
                data: null,
                status: ActionStatusEnum.ERROR,
                message: 'Test error message'
            };
            const spy = spyOn(component, 'updateSearchButtonStatus');
            // When
            component.manageNewConditions(conditions);
            // Then
            expect(mockToastrService.error).toHaveBeenCalledWith('Test error message');
            expect(spy).toHaveBeenCalledWith(StateButtonStatusEnum.ERROR);
        });

        it('should test UPDATED case', () => {
            // With
            component.cardsToShow = [];
            const conditions: AsyncResponseHandlerModel = {
                data: [
                    {
                        countryZip: {
                            countryCode: 'FR',
                            zipCode: '44000'
                        },
                        data: null,
                        icon: 'test/toto.png'
                    }
                ],
                status: ActionStatusEnum.UPDATED,
                message: null
            };
            // When
            component.manageNewConditions(conditions);
            // Then
            expect(mockToastrService.info).toHaveBeenCalledWith('Weather informations updated.');
            expect(component.cardsToShow).toEqual([
                {
                    countryZip: {
                        countryCode: 'FR',
                        zipCode: '44000'
                    },
                    data: null,
                    icon: 'test/toto.png'
                }
            ]);
        });

        it('should test DELETED case', () => {
            // With
            component.cardsToShow = [];
            const conditions: AsyncResponseHandlerModel = {
                data: [
                    {
                        countryZip: {
                            countryCode: 'FR',
                            zipCode: '44000'
                        },
                        data: null,
                        icon: 'test/toto.png'
                    }
                ],
                status: ActionStatusEnum.DELETED,
                message: {
                    countryCode: 'FR',
                    zipCode: '44360'
                }
            };
            const spy = spyOn(LocalStorageService.prototype, 'removeFromLocalStorage');
            // When
            component.manageNewConditions(conditions);
            // Then
            expect(mockToastrService.success).toHaveBeenCalledWith('Card was successfully deleted.');
            expect(spy).toHaveBeenCalledOnceWith({ countryCode: 'FR', zipCode: '44360' });
        });

        it('should test ADDED case', () => {
            // With
            component.cardsToShow = [];
            const conditions: AsyncResponseHandlerModel = {
                data: [
                    {
                        countryZip: {
                            countryCode: 'FR',
                            zipCode: '44000'
                        },
                        data: null,
                        icon: 'test/toto.png'
                    },
                    {
                        countryZip: {
                            countryCode: 'FR',
                            zipCode: '44360'
                        },
                        data: null,
                        icon: 'test/titi.png'
                    }
                ],
                status: ActionStatusEnum.ADDED,
                message: {
                    countryCode: 'FR',
                    zipCode: '44360'
                }
            };
            const spy = spyOn(LocalStorageService.prototype, 'addToLocalStorage');
            // When
            component.manageNewConditions(conditions);
            // Then
            expect(mockToastrService.success).toHaveBeenCalledWith('Card was successfully added.');
            expect(spy).toHaveBeenCalledOnceWith({ countryCode: 'FR', zipCode: '44360' });
        });

    });
});