/* tslint:disable:no-unused-variable */
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherObsService } from 'app/shared/services/weather.service';
import { SearchFormComponent } from './search-form.component';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFormComponent],
      imports: [HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init formGroup', () => {
    // With
    component.formGroup = null;
    // When
    component.ngOnInit();
    // Then
    expect(component.formGroup).toBeDefined();
  });

  it('zipCode should be required and numerical', () => {
    // With
    // When
    // Then
    expect(component.formGroup).toBeDefined();
    let errors = component.formGroup.get('zipCode').errors;
    expect(Object.keys(errors)).toEqual(['required']);
    // When
    component.formGroup.get('zipCode').setValue('a');
    // Then
    errors = component.formGroup.get('zipCode').errors;
    expect(Object.keys(errors)).toEqual(['pattern']);
    // When
    component.formGroup.get('zipCode').setValue('1');
    // Then
    errors = component.formGroup.get('zipCode').errors;
    expect(errors).toEqual(null);
  });

  it('country should be required and in list', () => {
    // With
    // When
    // Then
    expect(component.formGroup).toBeDefined();
    let errors = component.formGroup.get('country').errors;
    expect(Object.keys(errors)).toEqual(['required', 'isNotInList']);
    // When
    component.formGroup.get('country').setValue('toto');
    // Then
    errors = component.formGroup.get('country').errors;
    expect(Object.keys(errors)).toEqual(['isNotInList']);
    // When
    component.formGroup.get('country').setValue("Afghanistan");
    // Then
    errors = component.formGroup.get('country').errors;
    expect(errors).toEqual(null);
  });

  it('should filter on country input change', () => {
    // With
    const spy = spyOn(component, 'filterCountriesOptions');
    // When
    component.formGroup.get('country').setValue('toto');
    // Then
    expect(spy).toHaveBeenCalledWith('toto');
  });

  it('should test filterCountriesOptions', () => {
    // With
    component.allCountries = [
      {
        code: 'TO',
        name: 'toto'
      },
      {
        code: 'TI',
        name: 'titi'
      },
      {
        code: 'TA',
        name: 'Tato'
      }
    ];
    // When
    component.filterCountriesOptions('to');
    // Then
    expect(component.acpCountriesOptions).toEqual(['toto', 'Tato']);
    // When
    component.filterCountriesOptions('TITI');
    // Then
    expect(component.acpCountriesOptions).toEqual(['titi']);
    // When
    component.filterCountriesOptions('t');
    // Then
    expect(component.acpCountriesOptions).toEqual(['toto', 'titi', 'Tato']);
    // When
    component.filterCountriesOptions('test');
    // Then
    expect(component.acpCountriesOptions).toEqual([]);
  });

  it('should test country selection', () => {
    // With
    component.formGroup.get('country').setValue(null);
    component.acpCountriesOptions = ['France', 'Finlande'];
    // When
    component.onSelectCountry('France');
    // Then
    expect(component.acpCountriesOptions).toEqual([]);
    expect(component.formGroup.get('country').value).toEqual('France');
  });

  it('should test submit', () => {
    // With
    component.formGroup.get('zipCode').setValue(44800);
    component.onSelectCountry('France');
    const spy = spyOn(WeatherObsService.prototype, 'addToConditionsList');
    // When
    component.onSubmit();
    // Then
    expect(spy).toHaveBeenCalledOnceWith({ countryCode: 'FR', zipCode: '44800' }, true, false);
  });

});