import { Component, HostListener, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StateButtonStatusEnum } from 'app/shared/component/state-button/state-button.model';
import { CountriesConstant } from 'app/shared/constant/countries.constants';
import { CountryZipModel } from 'app/shared/models/country-zip.model';
import { WeatherObsService } from 'app/shared/services/weather.service';
import { isInListValidator } from 'app/shared/validators/is-in-list.directive';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnChanges, OnDestroy {

  formGroup: FormGroup;
  acpCountriesOptions: string[] = [];
  userCountry: string;
  allCountries = CountriesConstant.COUNTRY_LIST;
  souscription: Subscription;
  @Input() buttonStatus: StateButtonStatusEnum;

  @HostListener('document:click', ['$event'])
  clickout() {
    this.acpCountriesOptions = [];
  }

  constructor(private weatherService: WeatherObsService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.buttonStatus.currentValue === StateButtonStatusEnum.SUCCESS) {
      // Reinit fields after adding a card
      this.formGroup?.get('country').setValue(null);
      this.formGroup?.get('zipCode').setValue(null);
    }
  }

  ngOnInit(): void {
    // Init form
    this.formGroup = new FormGroup(
      {
        country: new FormControl(null, [Validators.required, isInListValidator(this.allCountries.map(c => c.name))]),
        zipCode: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')])
      }
    );
    // Listener for acp country filter
    this.souscription = this.formGroup.get('country').valueChanges.subscribe((newValue: string) => this.filterCountriesOptions(newValue));
  }

  /**
   * Filter the autocomplete options considering the value given as paramter
   * @param newValue user's input value
   */
  filterCountriesOptions(newValue: string): void {
    this.acpCountriesOptions = this.allCountries
      .map(c => c.name)
      .filter(country => country.toLowerCase().includes(newValue?.toLowerCase()));
  }

  /**
   * Country selection : change fc value and reinit acp options
   * @param selectedCountry 
   */
  onSelectCountry(selectedCountry: string): void {
    this.formGroup.get('country').setValue(selectedCountry);
    this.acpCountriesOptions = [];
  }

  onSubmit(): void {
    const countryName: string = this.formGroup.get('country').value;
    // Get the associated countryCode
    const countryCode: string = this.allCountries.find(country => country.name === countryName)?.code;
    const countryZip: CountryZipModel = {
      countryCode: countryCode,
      zipCode: this.formGroup.get('zipCode').value?.toString()
    };
    this.weatherService.addToConditionsList(countryZip, true, false);
  }

  ngOnDestroy(): void {
    this.souscription?.unsubscribe();
  }
}
