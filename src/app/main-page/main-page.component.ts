import { Component, OnDestroy, OnInit } from '@angular/core';
import { StateButtonStatusEnum } from 'app/shared/component/state-button/state-button.model';
import { ActionStatusEnum, AsyncResponseHandlerModel } from 'app/shared/models/async-response-handler.model';
import { ConditionModel } from 'app/shared/models/condition.model';
import { LocalStorageService } from 'app/shared/services/localstorage.service';
import { WeatherObsService } from 'app/shared/services/weather.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, OnDestroy {

  cardsToShow: any[] = [];
  searchButtonStatus: StateButtonStatusEnum = StateButtonStatusEnum.WAITING;
  souscription: Subscription;

  constructor(private localStorageService: LocalStorageService, private weatherService: WeatherObsService, private toasterService: ToastrService) { }

  ngOnInit(): void {
    // Subscribe to new conditions
    this.souscription = this.weatherService.getConditionsList().subscribe((conditions: AsyncResponseHandlerModel) => {
      this.manageNewConditions(conditions);
    });
  }

  /**
   * Manage async weather query result, depending on action asked
   * @param conditions object with query result, and action status
   */
  manageNewConditions(conditions: AsyncResponseHandlerModel): void {
    switch (conditions.status) {
      case ActionStatusEnum.ERROR:
        // Show Error button template
        this.updateSearchButtonStatus(StateButtonStatusEnum.ERROR);
        this.toasterService.error(conditions.message);
        break;
      case ActionStatusEnum.UPDATED:
        // Update displayed cards
        this.cardsToShow = conditions.data;
        // Show info toaster
        this.toasterService.info("Weather informations updated.")
        break;
      case ActionStatusEnum.DELETED:
        // Update displayed cards
        this.cardsToShow = conditions.data;
        // Remove from localStorage
        this.localStorageService.removeFromLocalStorage(conditions.message);
        this.toasterService.success("Card was successfully deleted.");
        break;
      case ActionStatusEnum.ADDED:
        // Update displayed cards
        this.cardsToShow = conditions.data;
        // Add to localStorage
        this.localStorageService.addToLocalStorage(conditions.message);
        // Show Success button template
        this.updateSearchButtonStatus(StateButtonStatusEnum.SUCCESS);
        this.toasterService.success("Card was successfully added.");
        break;
      default:
        // Do nothing
        break;
    }
  }

  /**
   * Remove displayed card from list and localStorage, if found
   * @param condition of card to remove
   */
  removeCard(condition: ConditionModel): void {
    this.weatherService.removeFromConditionsList(condition);
  }

  updateSearchButtonStatus(newValue: StateButtonStatusEnum): void {
    setTimeout(() => {
      this.searchButtonStatus = newValue;
      setTimeout(() => {
        this.searchButtonStatus = StateButtonStatusEnum.WAITING;
      }, 2000);
    }, 500);
  }

  ngOnDestroy(): void {
    this.souscription?.unsubscribe();
  }
}
